const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem, ipsum dolor sit, amet consectetur adipisicing elit. Ullam culpa animi deserunt ad, praesentium minima ipsam aliquid amet optio esse debitis modi rem officia ducimus laboriosam cum aliquam labore dolores.",
		price: 45000,
		onOffer: true
	},

	{
		id: "wdc002",
		name: "Phython - Django",
		description: "Lorem, ipsum dolor sit, amet consectetur adipisicing elit. Ullam culpa animi deserunt ad, praesentium minima ipsam aliquid amet optio esse debitis modi rem officia ducimus laboriosam cum aliquam labore dolores.",
		price: 50000,
		onOffer: true
	},

	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem, ipsum dolor sit, amet consectetur adipisicing elit. Ullam culpa animi deserunt ad, praesentium minima ipsam aliquid amet optio esse debitis modi rem officia ducimus laboriosam cum aliquam labore dolores.",
		price: 55000,
		onOffer: true
	},

	{
		id: "wdc004",
		name: "JavaScript - Angular",
		description: "Lorem, ipsum dolor sit, amet consectetur adipisicing elit. Ullam culpa animi deserunt ad, praesentium minima ipsam aliquid amet optio esse debitis modi rem officia ducimus laboriosam cum aliquam labore dolores.",
		price: 40000,
		onOffer: true
	}

]
export default coursesData;