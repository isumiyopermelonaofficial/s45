import React from 'react'
import Banner from '../components/Banner';
import Hightlights from '../components/Highlights';

export default function Home() {

	return (
		<>
			<Banner />
			<Hightlights />
		</>
	)

}