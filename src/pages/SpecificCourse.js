import React, {useState,useContext,useEffect} from 'react';
import {Container,Card,Button} from 'react-bootstrap';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';
import {useParams, useNavigate, Link} from 'react-router-dom';

export default function SpecificCourse(){
	const { courseId } = useParams();
	const navigate = useNavigate();

	useEffect(() => {
		fetch(`https://b165-shared-api.herokuapp.com/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	})

	const {user} = useContext(UserContext);
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('')

	// Enroll Function
	const enroll = (courseId) => {
		fetch(`https://b165-shared-api.herokuapp.com/users/enroll`, {
			method:'POST',
			headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title:'Successfully Enrolled',
					icon:'success'
				})
				navigate('/courses')
			} else {
				Swal.fire({
					title:'Something went wrong',
					icon: 'error'
				})
			}
		})
	}

	return (
		<Container>
			<Card>
				<Card.Header>
					<h4>{name}</h4>
				</Card.Header>
				<Card.Body>
					<Card.Text>{description}</Card.Text>
					<h6>Price: Php {price}</h6>
				</Card.Body>
				<Card.Footer>
				{user.accessToken !== null ?
					<div className="d-grip gap-2">
						<Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
					</div>		
					:
					<Link className="btn btn-warning d-grip gap-2" to="/login">Login to Enroll</Link>
				}
				</Card.Footer>
			</Card>
		</Container>
	)
}