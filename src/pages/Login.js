import React, {useState, useEffect, useContext} from 'react';
// useContext is used to unpack or deconstruct the value of the UserContext
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext.js';

export default function Login() {
	const navigate = useNavigate();

	const { user, setUser } = useContext(UserContext);

	// State Hooks
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState('');

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

/* Notes:
fetch is a method in JS, which allows to send a reques to an API and process its response.

Syntax:

fetch('url', {options}).then(response => response.json()).then(data => {console.log(data)})

url = the url coming form the API/Server
{optional object} = it contains additional information about our request such as methods, body and headers 

*/


/*

	function loginUser(e) {
		e.preventDefault();
		// Set email of the user to the local storage
		localStorage.setItem('email', email);

		// Set the global user state to ahve properties obtained from localStorage
		setUser({
			email: localStorage.getItem('email')
		})

		Swal.fire({
			title: "Login",
			icon: "success",
			text: `${email} Successfully Logged In`
		})
		clearFields();
	}
*/

	function clearFields() {
		setEmail('');
		setPassword('');
	}

function loginUser(e) {
	e.preventDefault();

	fetch('https://b165-shared-api.herokuapp.com/users/login', {
		method: 'POST',
		headers: {'Content-Type':'application/json'},
		body: JSON.stringify({
			email: email,
			password: password
		})
	})
	.then(response => response.json())
	.then(data => {
		console.log(data.accessToken)
		if(data.accessToken !== undefined) {
			localStorage.setItem('accessToken', data.accessToken);
			setUser({accessToken: data.accessToken});

			Swal.fire({
				title: "Login",
				icon: "success",
				text: `${email} Successfully Logged In`
			})

			fetch('https://b165-shared-api.herokuapp.com/users/details', {
				headers: {
					Authorization: `Bearer ${data.accessToken}`
				}
			})
			.then(response => response.json())
			.then(result => {
				console.log(result);

				if(result.isAdmin === true) {
					localStorage.setItem('isAdmin', result.isAdmin)
					localStorage.setItem('email', result.email)
					setUser({
						email: result.email,
						isAdmin: result.isAdmin
					})
					navigate('/courses')
				} else {
					navigate('/')
				}
			})

		} else {
			Swal.fire({
				title: 'Oops',
				icon: 'error',
				text: 'Something Went Wrong. Check your Credentials'
			})
		} //End of if
		clearFields();
	})
}


	return (
		// Conditional rendering statement that will redirect the user to the courses page when a user is logged in
		(user.accessToken !== null) ?
		<Navigate to="/courses" />
		:
		<Form className="mt-3" onSubmit={(e) => loginUser(e)}>
			<h1>Login</h1>
			<Form.Group>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)} 
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>
			{isActive ?
			<Button className="mt-2" variant="primary" type="submit">Login</Button>
			:
			<Button className="mt-2" variant="primary" type="submit" disabled>Login</Button>
			}
		</Form>
	)


}