import React, {useState, useEffect, useContext } from 'react';
import {Form,Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';
import { Navigate, useNavigate } from 'react-router-dom';

export default function Register() {
	const navigate = useNavigate();
	const {  user } = useContext(UserContext);
	
	// State hooks
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	//const [gender, setGender] = useState('');
	const [mobileNo, setMobileNo] = useState('');


	// Conditional Rendering
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both password match
		if((email !== '')) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [email])

	// Register User

	function registerUser(e) {
		e.preventDefault()
		fetch('https://b165-shared-api.herokuapp.com/users/register', {
			method: 'POST',
			headers: {'Content-Type':'application/json'},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password2,
				mobileNo: mobileNo
			})
		})
		.then(response => response.json())
		.then(result => {
			  if(result){
				
				Swal.fire({
					title:"Registered",
					icon: "success",
					text: `${email} Successfully Registered`
				})
				navigate('/')
			} else {
				Swal.fire({
					title:"Error",
					icon: "error",
					text: "Something Went Wrong, Please try again"
				})
			}
			clearFields();

		})

	}

	function clearFields() {
		setEmail('');
		setPassword1('');
		setPassword2('')
	}

	return (
		// Conditional Rendering
		(user.accessToken !== null) ?
		<Navigate to="/" />
		:
		<Form className="mt-3" onSubmit={(e) => registerUser(e)}>
			<h1>Register</h1>
			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter First Name"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)} 
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter Last Name"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)} 
				/>				
			</Form.Group>
			<Form.Group>
				<Form.Label>Email</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					required
					value={password1}
					onChange={e => setPassword2(e.target.value)}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Mobile No</Form.Label>
				<Form.Control
					type="number"
					placeholder="Enter Password"
					required
					value={password1}
					onChange={e => setMobileNo(e.target.value)}
				/>
			</Form.Group>
			{/*<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)}
				/>
			</Form.Group>*/}
			{isActive ?
			<Button className="mt-2" variant="primary" type="submit">Submit</Button>
			:
			<Button className="mt-2" variant="primary" type="submit" disabled>Submit</Button>
			}
			
		</Form>
	)
}