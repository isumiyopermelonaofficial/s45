import React, {useContext,useState,useEffect} from 'react';
//import coursesData from '../mockData/coursesData.js';
//import Course from '../components/Course.js';
import UserView from '../components/UserView.js';
import AdminView from '../components/AdminView.js';
import UserContext from '../UserContext';

export default function Courses() {
	// const courses = coursesData.map(course => {
	// 	return(
	// 		<Course key={course.id} courseProp={course}/>
	// 	)
	// });

	const {user} = useContext(UserContext);
	const [allCourses, setAllCourses] = useState([]);

	const fetchData = () => {
		fetch('https://b165-shared-api.herokuapp.com/courses/')
		.then(response => response.json())
		.then(data => {
			//console.log(data)
			setAllCourses(data)
		})
	}
	
	useEffect(() => {
		fetchData();
	}, [])

	return (
		<>
		{(user.isAdmin === true) ?
			<AdminView coursesData={allCourses} fetchData={fetchData}/>
			:
			<UserView coursesData={allCourses}/>
		}
		</>
	)
}