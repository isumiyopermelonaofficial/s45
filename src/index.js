import React from 'react';
import ReactDOM from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';


// ReactJS does not like rendering two adjacent elements. Instead the adjacent elements must be wrapped by a parent element or React Fragments

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <App />
);

// JSX is a syntax used in React JS
// Javascript + XML - Extension of JS that let's us create objects which will be then compiled and added as HTML elements

// With JSX, we are able to create HTML elements using JS