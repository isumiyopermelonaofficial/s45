import React from 'react';
import {Row,Col,Card} from 'react-bootstrap';

export default function Hightlights() {
	return (
		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn from Home</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem velit deserunt obcaecati fugit, alias et praesentium consectetur aliquid, veniam unde ea consequuntur doloremque incidunt totam molestiae voluptatum temporibus perspiciatis, cumque?
						</Card.Text>
					</Card.Body>	
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem velit deserunt obcaecati fugit, alias et praesentium consectetur aliquid, veniam unde ea consequuntur doloremque incidunt totam molestiae voluptatum temporibus perspiciatis, cumque?
						</Card.Text>
					</Card.Body>	
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of Our Community</h2>
						</Card.Title>
						<Card.Text>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem velit deserunt obcaecati fugit, alias et praesentium consectetur aliquid, veniam unde ea consequuntur doloremque incidunt totam molestiae voluptatum temporibus perspiciatis, cumque?
						</Card.Text>
					</Card.Body>	
				</Card>	
			</Col>
		</Row>

	)

}