import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCourse({course, isActive, fetchData}) {

	const archiveToggle = (courseId) => {
		fetch(`https://young-atoll-38162.herokuapp.com/courses/${courseId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully disabled'
				})
				fetchData()
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData()
			}


		})
	}

	const unArchiveToggle = (courseId) => {
		fetch(`https://young-atoll-38162.herokuapp.com/${courseId}/unarchive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully Activated'
				})
				fetchData()
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'Error',
					text: 'Please Try again'
				})
				fetchData()
			}


		})
	}
 
	return(
		<>
			{isActive ?
				<Button variant="danger" size="sm" onClick={() => archiveToggle(course)}>Archive</Button>
				:
				<Button variant="success" size="sm" onClick={() => unArchiveToggle(course)}>Unarchive</Button>
			}
		</>

		)
}