import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function DeleteCourse(course, fetchData){
	
	const deleteCourse = (courseId) => {
		fetch(`https://stark-river-45927.herokuapp.com/courses/${courseId}`, {
			method: 'DEL',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course Successfully Deleted'
				})
				fetchData()
			}else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please Try again'
				})
				fetchData()
			}

		})
	}

	return (
		<Button variant="danger" size="sm" onClick={() => deleteCourse(course)}>Delete</Button>
	)
}