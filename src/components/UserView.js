import React, {useState,useEffect} from 'react';
import Course from './Course.js';

export default function UserView({coursesData}) {
	const [courses, setCourses] = useState([])
	
	useEffect(() => {
		const coursesArray = coursesData.map(course => {
			//Render Active Courses
			if(course.isActive === true) {
				return (
					<Course courseProp={course} key={course._id}/>
				)
			} else {
				return null;
			}
		})
		setCourses(coursesArray)
	},[coursesData])


	return (
		<>
			{courses}
		</>
	)
} 