import React, {useState, useEffect} from 'react';
//import Course from './Course.js';
import {Table} from 'react-bootstrap';
import AddCourse from './AddCourse.js';
import EditCourse from './EditCourse.js';
import ArchiveCourse from './ArchiveCourse.js';
import DeleteCourse from './DeleteCourse.js';

export default function AdminView(props) {

	const {coursesData,fetchData} = props
	const [courses, setCourses] = useState([])

	useEffect(() => {
		const coursesArray = coursesData.map(course => {
			return(
				<tr key={course._id}>
					<td>{course._id}</td>
					<td>{course.name}</td>
					<td>{course.description}</td>
					<td>{course.price}</td>
					<td className={course.isActive ? "text-success":"text-danger"}>
						{course.isActive ? "Available":"Unavailable"}
					</td>
					<td><EditCourse course={course._id} fetchData={fetchData}/></td>
					<td><ArchiveCourse course={course._id} isActive={course.isActive} fetchData={fetchData}/></td>
					<td><DeleteCourse course={course._id} fetchData={fetchData}/></td>
				</tr>
			)
		})
		setCourses(coursesArray)
	}, [coursesData, fetchData])

	return (
		<>
			<div className="text-center my-4">
				<h1>Admin Dashboard</h1>
				<AddCourse fetchData={fetchData}/>
			</div>
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th colspan="3">Actions</th>
					</tr>
				</thead>
				<tbody>
					{courses}
				</tbody>
			</Table>
		</>
	)
}