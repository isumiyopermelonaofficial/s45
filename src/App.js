import React, { useState } from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home.js';
import Courses from './pages/Courses.js';
import { Container } from 'react-bootstrap';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import Error404 from './pages/Error404.js';
import SpecificCourse from './pages/SpecificCourse.js';
import { UserProvider } from './UserContext.js';

// For Routes
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

// The Router(BrowserRouter) component will enable us tonpm  simulate page navigation by synchronizing the shown content and the shown URL in the web browser

// The Routes(before it was called Switch) declares the Route we can go to.

function App() {

  // React Context is nothing but a global state to the app. Is is a way to make a particular data available to all components no matter how they are nested. Context Helps you broadcast data and changes happening to the data
  const [user, setUser] = useState({
    email: localStorage.getItem('email'),
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  // Function for clearing localStorage
  const unsetUser = () => {
    localStorage.clear()
  }

  return (
  <UserProvider value={{user, setUser, unsetUser}}>
  <Router>
   <AppNavbar />
   <Container>
    <Routes>
   	  < Route path="/" element={ <Home /> }/>
      < Route path="/courses" element={ <Courses /> }/>
      < Route path="/login" element={ <Login /> }/>
      < Route path="/logout" element={ <Logout /> }/>
      < Route path="/register" element={<Register />}/>
      < Route path="/courses/:courseId" element={<SpecificCourse />} />
      < Route path="*" element={<Error404 />} />
    </Routes>
   </Container>
  </Router>
  </UserProvider>
  );
}

export default App;